package com.john.toolbarbase.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.john.toolbarbase.R;
import com.john.toolbarbase.infrastructure.GetJsonData;
import com.john.toolbarbase.infrastructure.JsonDataContainer;
import com.john.toolbarbase.views.MainNavDrawer;
import com.john.toolbarbase.views.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class JsonActivity extends BaseActivity {
    private static final String LOG_TAG=JsonActivity.class.getSimpleName();

    private List<JsonDataContainer> jsonDataContainers=new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onMyOnCreate() {
        setContentView(R.layout.activity_json);
        getSupportActionBar().setTitle("Json Activity");
        setNavDrawer(new MainNavDrawer(this));

        recyclerView=(RecyclerView)findViewById(R.id.activity_json_recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ProcessDataJson processDataJson=new ProcessDataJson("google", true);
        processDataJson.execute();

        //This is a test Please see the Log.v tag = "Data returned was"
//        GetRawData getRawData=new GetRawData("https://api.flickr.com/services/feeds/photos_public.gne?tags=android,lollipop&format=json&nojsoncallback=1");
//        getRawData.execute();
    }


    private String getSavedPreferenceData(String key) {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sharedPreferences.getString(key, "");
    }

    private class ProcessDataJson extends GetJsonData{
        public ProcessDataJson(String query, boolean matchAll) {
            super(query, matchAll);
        }

        @Override
        public void execute() {
            ProcessData processData=new ProcessData();
            processData.execute();
        }

        public class ProcessData extends DownloadJsonData{
            @Override
            protected void onPostExecute(String wData) {
                super.onPostExecute(wData);
                recyclerViewAdapter=new RecyclerViewAdapter(JsonActivity.this, getJsonDataContainers());
                recyclerView.setAdapter(recyclerViewAdapter);
            }
        }
    }
}//end class






























