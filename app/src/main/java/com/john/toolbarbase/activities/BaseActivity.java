package com.john.toolbarbase.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.john.toolbarbase.R;
import com.john.toolbarbase.views.NavDrawer;

public abstract class BaseActivity extends AppCompatActivity {
    public static final String JSON_QUERY="JSON_QUERY";

    protected Toolbar toolbar;
    protected NavDrawer navDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onMyOnCreate();
    }

    protected abstract void onMyOnCreate();

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        toolbar=(Toolbar)findViewById(R.id.include_toolbar);
        if(toolbar!=null){
            setSupportActionBar(toolbar);
        }
    }

    protected void setNavDrawer(NavDrawer navDrawer){
        this.navDrawer=navDrawer;
        this.navDrawer.create();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
