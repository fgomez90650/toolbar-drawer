package com.john.toolbarbase.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.john.toolbarbase.R;
import com.john.toolbarbase.dialogs.FoodDialog;
import com.john.toolbarbase.dialogs.TheFragmentDialog;
import com.john.toolbarbase.infrastructure.Food;
import com.john.toolbarbase.views.CardViewVerticalAdapter;
import com.john.toolbarbase.views.MainNavDrawer;

public class MainActivity extends BaseActivity implements CardViewVerticalAdapter.OnListener {

    private String[] foods;
    private int[] foodImgIds;

    @Override
    protected void onMyOnCreate() {
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle(getResources().getString(R.string.main_activity));
        setNavDrawer(new MainNavDrawer(this));

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.activity_main_recyclerView);

        //This could go on the BaseActivity but for this lesson I wanted to keep it here
        foods = new String[Food.foods.length];
        foodImgIds = new int[Food.foods.length];
        for(int i=0; i< foods.length; i++){
            foods[i]=Food.foods[i].getName();
            foodImgIds[i]=Food.foods[i].getImgResourceId();
        }

        GridLayoutManager layoutManager=new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        CardViewVerticalAdapter adapter=new CardViewVerticalAdapter(this, foods, foodImgIds);
        recyclerView.setAdapter(adapter);
        adapter.setListener(this);
    }

    @Override
    public void onClick(int position) {
        FragmentTransaction fragmentTransaction=getSupportFragmentManager()
                .beginTransaction().addToBackStack(null);
        FoodDialog dialog=new FoodDialog();
        Bundle bundle=new Bundle();
        bundle.putString(FoodDialog.FOOD_NAME, foods[position]);
        bundle.putInt(FoodDialog.FOOD_ID, foodImgIds[position]);
        dialog.setArguments(bundle);
        dialog.show(fragmentTransaction, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_dialog:
                FragmentTransaction fragmentTransaction=getSupportFragmentManager()
                        .beginTransaction().addToBackStack(null);
                TheFragmentDialog dialog=new TheFragmentDialog();
                dialog.show(fragmentTransaction, null);
                return true;
        }
        return false;
    }


}
