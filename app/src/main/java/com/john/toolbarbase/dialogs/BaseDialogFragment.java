package com.john.toolbarbase.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * this base class is for good practice
 * for example do not repeat yourself
 */
public class BaseDialogFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
