package com.john.toolbarbase.dialogs;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

import com.john.toolbarbase.R;

public class FoodDialog extends BaseDialogFragment {
    public static final String FOOD_NAME="FOOD_NAME";
    public static final String FOOD_ID="FOOD_ID";

    private String food;
    private int foodImgId;
    private ImageView imageView;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle=getArguments();
        food=bundle.getString(FOOD_NAME);
        foodImgId=bundle.getInt(FOOD_ID);

        View dialogView=getActivity().getLayoutInflater().inflate(R.layout.food_dialog, null, false);
        imageView=(ImageView)dialogView.findViewById(R.id.food_dialog_image);
        Drawable drawable=dialogView.getResources().getDrawable(foodImgId);
        imageView.setImageDrawable(drawable);

        AlertDialog dialog=new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle(food)
                .setPositiveButton("OK", null)
                .show();

        return dialog;
    }
}
