package com.john.toolbarbase.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.john.toolbarbase.R;
import com.john.toolbarbase.infrastructure.JsonDataContainer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapterViewHolder>{
    private static final String LOG_TAG = RecyclerViewAdapter.class.getSimpleName();

    List<JsonDataContainer> jsonDataContainers;
    private Context context;

    public RecyclerViewAdapter(Context context, List<JsonDataContainer> jsonDataContainers) {
        this.context=context;
        this.jsonDataContainers = jsonDataContainers;
    }

    @Override
    public RecyclerViewAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_item, null);
        RecyclerViewAdapterViewHolder recyclerViewAdapterViewHolder=new RecyclerViewAdapterViewHolder(view);

        return recyclerViewAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapterViewHolder holder, int position) {
        JsonDataContainer jsonDataContainer=jsonDataContainers.get(position);
        Log.d(LOG_TAG, "Processing: " + jsonDataContainer.getTitle() + " ---> " + Integer.toString(position));

        //Picasso does the thread
        Picasso.with(context).load(jsonDataContainer.getImage())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(holder.imageView);
        holder.textView.setText(jsonDataContainer.getTitle());
    }

    @Override
    public int getItemCount() {
        return (jsonDataContainers!=null ? jsonDataContainers.size() : 0);
    }

}



















