package com.john.toolbarbase.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.john.toolbarbase.R;

public class RecyclerViewAdapterViewHolder extends RecyclerView.ViewHolder {
    protected ImageView imageView;
    protected TextView textView;

    public RecyclerViewAdapterViewHolder(View itemView) {
        super(itemView);
        this.imageView=(ImageView)itemView.findViewById(R.id.list_view_image);
        this.textView=(TextView)itemView.findViewById(R.id.list_view_text);
    }
}
