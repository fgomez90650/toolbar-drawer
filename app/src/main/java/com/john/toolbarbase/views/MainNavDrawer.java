package com.john.toolbarbase.views;

import android.view.View;
import android.widget.Toast;

import com.john.toolbarbase.R;
import com.john.toolbarbase.activities.BaseActivity;
import com.john.toolbarbase.activities.JsonActivity;
import com.john.toolbarbase.activities.ListActivity1;
import com.john.toolbarbase.activities.MainActivity;
import com.john.toolbarbase.activities.RecycleActivity;

public class MainNavDrawer extends NavDrawer {
    public MainNavDrawer(final BaseActivity baseActivity) {
        super(baseActivity);
        //For the top on the Drawer
        addItem(new ActivityNavDrawerItem(MainActivity.class,
                baseActivity.getResources().getString(R.string.main_activity),
                null, R.mipmap.ic_launcher,
                R.id.include_nav_drawer_topItems));

        addItem(new ActivityNavDrawerItem(ListActivity1.class,
                baseActivity.getResources().getString(R.string.list_activity1),
                null, R.drawable.ic_action_group,
                R.id.include_nav_drawer_topItems));

        addItem(new ActivityNavDrawerItem(RecycleActivity.class,
                baseActivity.getResources().getString(R.string.recyler_activity),
                null, R.mipmap.ic_launcher,
                R.id.include_nav_drawer_topItems));

        addItem(new ActivityNavDrawerItem(JsonActivity.class,
                "Json Activity",
                null, R.mipmap.ic_launcher,
                R.id.include_nav_drawer_topItems));

        //For the Bottom on the Drawer
        addItem(new BasicNavDrawerItem("Bottom Line", null, R.mipmap.ic_launcher, R.id.include_nav_drawer_bottomItems){
            @Override
            public void onClick(View view) {
                Toast.makeText(baseActivity, "This is the Bottom event", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
