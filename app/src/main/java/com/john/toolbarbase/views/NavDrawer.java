package com.john.toolbarbase.views;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.john.toolbarbase.R;
import com.john.toolbarbase.activities.BaseActivity;

import java.util.ArrayList;

public class NavDrawer {
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerItem selectedNavDrawerItem;

    protected BaseActivity baseActivity;
    protected DrawerLayout drawerLayout;
    protected ViewGroup navDrawerView;

    public NavDrawer(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        navDrawerItems=new ArrayList<>();
        drawerLayout=(DrawerLayout)baseActivity.findViewById(R.id.activity_main_drawer_layout);
        navDrawerView=(ViewGroup)baseActivity.findViewById(R.id.nav_drawer);

        //just in case someone does not implement it
        if(drawerLayout==null || navDrawerView==null)
            throw new RuntimeException("Please have views with the ids of drawer_layout and nav_drawer (example on MainNavDrawer)");

        Toolbar toolbar=baseActivity.getToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOpen(!isOpen());
            }
        });
    }

    public boolean isOpen(){
        return drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    public void addItem(NavDrawerItem item){
        navDrawerItems.add(item);
        item.navDrawer=this;
    }

    //this one open and close the drawer
    public void setOpen(boolean isOpen){
        if(isOpen)
            drawerLayout.openDrawer(GravityCompat.START);
        else
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void setSelectedNavDrawerItem(NavDrawerItem selectedNavDrawerItem) {
        if(this.selectedNavDrawerItem!=null)
            this.selectedNavDrawerItem.setSelected(false);
        this.selectedNavDrawerItem = selectedNavDrawerItem;
        this.selectedNavDrawerItem.setSelected(true);
    }

    public void create(){
        LayoutInflater inflater=baseActivity.getLayoutInflater();
        for(NavDrawerItem item : navDrawerItems){
            item.inflate(inflater, this.navDrawerView);
        }
    }

    public static abstract class NavDrawerItem{
        protected NavDrawer navDrawer;

        public abstract void inflate(LayoutInflater inflater, ViewGroup navDrawerView);
        public abstract void setSelected(boolean isSelected);
    }

    public static class BasicNavDrawerItem extends NavDrawerItem implements View.OnClickListener{
        private String text;
        private String badge;
        private int iconDrawable;
        private int containerId;

        private ImageView iconImage;
        private TextView textView;
        private TextView badgeTextView;
        private View view;
        private int defaultTextColor;

        public BasicNavDrawerItem(String text, String badge, int iconDrawable, int containerId) {
            this.text = text;
            this.badge = badge;
            this.iconDrawable = iconDrawable;
            this.containerId = containerId;
        }

        @Override
        public void inflate(LayoutInflater inflater, ViewGroup navDrawerView) {
            ViewGroup container=(ViewGroup)navDrawerView.findViewById(containerId);
            if(container==null)
                throw new RuntimeException("View not found on Inflate: "+text+" "+getClass().getSimpleName());

            view=inflater.inflate(R.layout.list_item_nav_drawer, container, false);
            container.addView(view);
            view.setOnClickListener(this);

            iconImage=(ImageView)view.findViewById(R.id.list_item_nav_drawer_icon);
            textView=(TextView)view.findViewById(R.id.list_item_nav_drawer_text);
            badgeTextView=(TextView)view.findViewById(R.id.list_item_nav_drawer_badge);
            defaultTextColor=textView.getCurrentTextColor();

            iconImage.setImageResource(iconDrawable);
            textView.setText(text);
            if(badge!=null)
                badgeTextView.setText(badge);
            else
                badgeTextView.setVisibility(View.GONE);

        }

        @Override
        public void onClick(View v) {
            navDrawer.setSelectedNavDrawerItem(this);
        }

        @Override
        public void setSelected(boolean isSelected) {
            if(isSelected){
                view.setBackgroundResource(R.drawable.item_selected_background);
                textView.setTextColor(navDrawer.baseActivity.getResources().getColor(R.color.list_item_nav_drawer_selected_item_text_color));
            }else {
                view.setBackground(null);
                textView.setTextColor(defaultTextColor);//meaning the current color
            }
        }

        public void setText(String text) {
            this.text = text;
            if(view!=null)
                textView.setText(text);
        }

        public void setBadge(String badge) {
            this.badge = badge;
            if(view!=null)
                badgeTextView.setVisibility(View.VISIBLE);
            else
                badgeTextView.setVisibility(View.GONE);
        }

        public void setIconDrawable(int iconDrawable) {
            this.iconDrawable = iconDrawable;
            if(view!=null)
                iconImage.setImageResource(iconDrawable);
        }

    }

    public static class ActivityNavDrawerItem extends BasicNavDrawerItem{
        private final Class targetActivity;

        public ActivityNavDrawerItem(Class targetActivity, String text, String badge, int iconDrawable, int containerId) {
            super(text, badge, iconDrawable, containerId);
            this.targetActivity=targetActivity;
        }

        @Override
        public void inflate(LayoutInflater inflater, ViewGroup navDrawerView) {
            super.inflate(inflater, navDrawerView);
            if(navDrawer.baseActivity.getClass()==targetActivity)
                navDrawer.setSelectedNavDrawerItem(this);
        }

        @Override
        public void onClick(View view) {
            navDrawer.setOpen(false);
            if(navDrawer.baseActivity.getClass()==targetActivity)
                return;
            super.onClick(view);

            navDrawer.baseActivity.startActivity(new Intent(navDrawer.baseActivity, targetActivity));
            navDrawer.baseActivity.finish();
        }
    }
}



















