package com.john.toolbarbase.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.john.toolbarbase.R;
import com.john.toolbarbase.activities.ListActivity1;
import com.john.toolbarbase.infrastructure.Food;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Food> {
    Context context;
    ViewHolder viewHolder;
    Food food;
    ListView listView;

    //this could be done better
    public ListViewAdapter(Context context, Food[] foods, ListView listView) {
        super(context, R.layout.list_view_item, foods);
        this.context=context;
        this.listView=listView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder=null;
        food=getItem(position);

        LayoutInflater layoutInflater=(LayoutInflater.from(context));
        if(convertView==null){
            convertView=layoutInflater.inflate(R.layout.list_view_item, parent, false);
            viewHolder=new ViewHolder();
            viewHolder.imageView=(ImageView)convertView.findViewById(R.id.list_view_image);
            viewHolder.textView=(TextView)convertView.findViewById(R.id.list_view_text);
            convertView.setTag(viewHolder);
        }else{
            viewHolder=(ViewHolder)convertView.getTag();
            convertView.clearAnimation();
            if(listView.isItemChecked(position)){
                convertView.setTranslationX(ListActivity1.SELECTED_ITEM_TRANSLATION_X);
            }else {
                convertView.setTranslationX(0);
            }
        }
        Drawable drawable=convertView.getResources().getDrawable(food.getImgResourceId());
        viewHolder.imageView.setImageDrawable(drawable);
        viewHolder.textView.setText(food.getName());

        return convertView;
    }

    public class ViewHolder{
        public ImageView imageView;
        public TextView textView;
    }
}
