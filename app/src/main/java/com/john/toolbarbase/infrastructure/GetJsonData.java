package com.john.toolbarbase.infrastructure;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetJsonData extends GetRawData {
    private static final String LOG_TAG=GetJsonData.class.getSimpleName();
    private List<JsonDataContainer> jsonDataContainers;
    private Uri destinationUri;

    public GetJsonData(String searchCriteria, boolean matchAll) {
        super(null);
        createAndUpdateUri(searchCriteria, matchAll);
        jsonDataContainers =new ArrayList<>();
    }

    public List<JsonDataContainer> getJsonDataContainers() {
        return jsonDataContainers;
    }

    private boolean createAndUpdateUri(String searchCriteria, boolean matchAll) {
        final String API_BASE_URL = "https://api.flickr.com/services/feeds/photos_public.gne";
        final String TAGS_PARAM = "tags";
        final String TAGMODE_PARAM = "tagmode";
        final String FORMAT_PARAM = "format";
        final String NO_JSON_CALLBACK_PARAM = "nojsoncallback";

        destinationUri=Uri.parse(API_BASE_URL).buildUpon()
                .appendQueryParameter(TAGS_PARAM, searchCriteria)
                .appendQueryParameter(TAGMODE_PARAM, matchAll ? "ALL" : "ANY")
                .appendQueryParameter(FORMAT_PARAM, "json")
                .appendQueryParameter(NO_JSON_CALLBACK_PARAM, "1")
                .build();

        return destinationUri!=null;
    }


    @Override
    public void execute() {
        super.setRawUrl(destinationUri.toString());
        DownloadJsonData downloadJsonData=new DownloadJsonData();
        Log.v(LOG_TAG, "Hyuk Built URI = "+destinationUri.toString());
        downloadJsonData.execute(destinationUri.toString());
    }

    public class DownloadJsonData extends DownloadRawData {

        @Override
        protected String doInBackground(String... params) {
            String[] paras={destinationUri.toString()};
            return super.doInBackground(paras);
        }

        @Override
        protected void onPostExecute(String wData) {
            super.onPostExecute(wData);
            processResult();
        }
    }

    private void processResult() {
        if(getDownloadStatus()!=DownloadStatus.OK){
            Log.e(LOG_TAG, "Error downloading raw file");
        }

        final String ITEMS = "items";
        final String TITLE = "title";
        final String AUTHOR = "author";
        final String AUTHOR_ID = "author_id";
        final String TAGS = "tags";

        final String MEDIA = "media";
        final String PHOTO_URL = "m";

        try {
            JSONObject jsonObjectData=new JSONObject(getData());
            JSONArray jsonArrayItems=jsonObjectData.getJSONArray(ITEMS);
            for(int i=0; i<jsonArrayItems.length(); i++){
                JSONObject jsonObjectAll=jsonArrayItems.getJSONObject(i);
                String title=jsonObjectAll.getString(TITLE);
                String author=jsonObjectAll.getString(AUTHOR);
                String authorId=jsonObjectAll.getString(AUTHOR_ID);
                String tags=jsonObjectAll.getString(TAGS);

                JSONObject jsonObjectMedia=jsonObjectAll.getJSONObject(MEDIA);
                String phtoUrl=jsonObjectMedia.getString(PHOTO_URL);
                String link=phtoUrl.replaceFirst("_m.", "_b.");

                JsonDataContainer jsonDataContainerObject =new JsonDataContainer(title, author, authorId, link, tags, phtoUrl);

                this.jsonDataContainers.add(jsonDataContainerObject);

                for(JsonDataContainer jsonDataContainer : jsonDataContainers){
                    Log.v(LOG_TAG, jsonDataContainer.toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Error processing Json");
        }
    }

}//end class



















