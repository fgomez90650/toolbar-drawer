package com.john.toolbarbase.infrastructure;

import com.john.toolbarbase.R;

public class Food {
    private String name;
    private int imgResourceId;

    public static final Food[] foods={
        new Food("Beef", R.drawable.beef),
        new Food("Fruit", R.drawable.fruit),
        new Food("Hamburger", R.drawable.hamburger),
        new Food("Healthy Food", R.drawable.healty_food),
        new Food("Korean food", R.drawable.korean_food),
        new Food("Pizza", R.drawable.pizza),
        new Food("Soup", R.drawable.soup),
        new Food("Sub", R.drawable.sub)
    };

    private Food(String name, int imgResourceId) {
        this.name = name;
        this.imgResourceId = imgResourceId;
    }

    public String getName() {
        return name;
    }

    public int getImgResourceId() {
        return imgResourceId;
    }
}
