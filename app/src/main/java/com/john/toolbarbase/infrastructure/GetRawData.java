package com.john.toolbarbase.infrastructure;


import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

enum DownloadStatus{
    IDLE,
    PROCESSING,
    NOT_INITIALISED,
    FAILED_OR_EMPTY,
    OK
}

public class GetRawData {
    private String LOG_TAG=getClass().getSimpleName();
    private String rawUrl;
    private String data;
    private DownloadStatus downloadStatus;

    public GetRawData(String rawUrl) {
        this.rawUrl = rawUrl;
        this.downloadStatus=DownloadStatus.IDLE;
    }

    public void reset(){
        this.downloadStatus=DownloadStatus.IDLE;
        this.rawUrl=null;
        this.data=null;
    }

    public String getData() {
        return data;
    }

    public DownloadStatus getDownloadStatus() {
        return downloadStatus;
    }

    public void setRawUrl(String rawUrl) {
        this.rawUrl = rawUrl;
    }

    public void execute(){
        this.downloadStatus=DownloadStatus.PROCESSING;
        DownloadRawData downloadRawData=new DownloadRawData();
        downloadRawData.execute(rawUrl);

    }

    public class DownloadRawData extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection=null;
            BufferedReader reader=null;

            if(params==null)
                return null;

            try {

                URL url=new URL(params[0]);
                urlConnection=(HttpURLConnection)url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream=urlConnection.getInputStream();
                if(inputStream==null)
                    return null;
                reader=new BufferedReader(new InputStreamReader(inputStream));

                //StringBuilder is also not thread safe
                StringBuffer stringBuffer=new StringBuffer();
                String line;
                while ((line=reader.readLine())!=null){
                    stringBuffer.append(line+"\n");
                }

                return stringBuffer.toString();

            } catch (MalformedURLException e) {
                Log.e(LOG_TAG, "Hyuk: ", e);
                return null;
            } catch (IOException e) {
                Log.e(LOG_TAG, "Hyuk: ", e);
                return null;
            } finally {
                if(urlConnection!=null)
                    urlConnection.disconnect();
                if(reader!=null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.e(LOG_TAG, "Hyuk Error Closing Stream", e);
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(String wData) {
            data=wData;
            Log.v(LOG_TAG, "Data returned was: "+data);
            if(data==null){
                downloadStatus=DownloadStatus.NOT_INITIALISED;
                if(rawUrl==null){
                    downloadStatus=DownloadStatus.NOT_INITIALISED;
                }else {
                    downloadStatus=DownloadStatus.FAILED_OR_EMPTY;
                }
            }else {
                //Success
                downloadStatus=DownloadStatus.OK;
            }
        }
    }
}


















